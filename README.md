# Nephilid Engineering

## Project Description

This is the website for Nephilid Engineering. The purpose of Nephilid Engineering is to bridge the gap between the coding knowledge recent Microverse graduates have and the development experience they need.

<!-- ![screenshot](public/images/screenshot1.png)
![screenshot](public/images/screenshot2.png) -->

#### Features
-   Responsive layout works well on mobile or desktop
-   About page for sharing member info and related links

###### Coming Features
-   Member login to display unique content for prospective employers

## Built With
-   Ruby / Ruby on Rails
-   JavaScript ES6 / HAML / SCSS
-   Bootstrap

## Live Demo
#### Deployed on: HEROKU
Try it out [here]()


## Getting Started Locally
### Prerequisites
To get this project up and running locally, you must have or be able to install the necessary packages to run Ruby on Rails on your computer.

**To get this project set up on your local machine, follow these simple steps:**

**Step 1**<br>
Navigate to the local folder where you want the repository to be cloned into and run the following command:
```
git clone https://gitlab.com/defoe.brand/nephilid-engineering.git
```

**Step 2**<br>
Next, enter the correct folder using this command:
```
cd nephilid-engineering
```

**Step 3**<br>
Copy the database.example.yml file and name it database.yml but do not remove the example file.

**Step 4**<br>
Run the following command to install gems
```
bundle install
```

**Step 5**<br>
Next, run the next command
```
yarn install
```

**Step 6**<br>
Then, begin the server by typing this into the terminal:
```
rails s
```

****

**Step 7**<br>
Finally, open the browser of your choice and navigate to the homepage to see the application running by typing this into the address bar:
```
http://localhost:3000/
```

### Testing
This project uses RSpec for testing Rails features.
> To run tests open a terminal inside the project's root directory and enter `rails test`

## Linters
To run rubocop and haml-lint use:
```
yarn run lint
```

## 🤝 Contributing

Our favorite contributions are those that help us improve the project, so please leave an issue or a feature request if you have any suggestions!

Feel free to check the [issues page](https://gitlab.com/defoe.brand/nephilid-engineering/-/issues) to either create an issue or help us out by fixing an existing one.

Also feel free to leave an issue if you have resources you would like to see added!

## Show your support

If you've read this far....give us a ⭐️!

## :clap: Acknowledgements

-   Microverse
