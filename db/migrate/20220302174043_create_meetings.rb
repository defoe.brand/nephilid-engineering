class CreateMeetings < ActiveRecord::Migration[6.1]
  def change
    create_table :meetings do |t|
      t.references :host
      t.datetime :meeting_time
      t.text :meeting_url
      t.text :challenge_link

      t.bigint :user_ids, array: true, default: []
      
      t.timestamps
    end
    
    remove_column :hosts, :challenge_link, :string
    remove_column :hosts, :next_meeting, :datetime
    remove_column :hosts, :meeting_url, :string
    add_column :hosts, :meeting_day, :int, default: 5
    add_column :hosts, :meeting_time, :jsonb, default: { hour: 0, minute: 0 }
  end
  # 
  # def up
  # end
  # 
  # def down
  #   add_column :hosts, :challenge_link, :string
  #   add_column :hosts, :meeting_time, :datetime
  #   add_column :hosts, :meeting_url, :string
  # end
end
