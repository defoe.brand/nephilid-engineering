class AddTimezoneToHosts < ActiveRecord::Migration[6.1]
  def change
    add_column :hosts, :timezone, :string
  end
end
