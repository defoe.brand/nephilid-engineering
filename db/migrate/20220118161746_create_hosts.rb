class CreateHosts < ActiveRecord::Migration[6.1]
  def change
    create_table :hosts do |t|
      t.datetime :next_meeting
      t.text :meeting_url

      t.references :user, null: false, foreign_key: true
      t.timestamps
    end
  end
end