class CreateMembers < ActiveRecord::Migration[6.1]
  def change
    create_table :members do |t|
      t.string :name
      t.string :gender
      t.string :title
      t.string :avatar
      t.string :facebook
      t.string :twitter
      t.string :linkedin
      t.string :instagram
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
