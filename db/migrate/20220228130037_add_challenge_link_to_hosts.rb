class AddChallengeLinkToHosts < ActiveRecord::Migration[6.1]
  def change
    add_column :hosts, :challenge_link, :string
  end
end
