class Host < ApplicationRecord
  belongs_to :user
  validates :timezone, presence: true
  
  scope :next_meeting, ->(host) { Meeting.where('host_id = ? AND meeting_time > ?', host.id, (DateTime.now - 55.minutes)).order(meeting_time: :asc).limit(1) }

  
  has_many :meetings
end
