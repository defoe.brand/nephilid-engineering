class Meeting < ApplicationRecord
  has_one :host
  validates :meeting_time, :meeting_url, presence: true
  validates :meeting_time, uniqueness: true
  
  scope :coming_meetings, -> { where('meeting_time > ?', (DateTime.now - 55.minutes)).order(meeting_time: :asc) }
  scope :next_meeting, ->(host) { where('host_id = ? AND meeting_time > ?', host.id, (DateTime.now - 55.minutes)).order(meeting_time: :asc).limit(1) }
  
  scope :new_meeting, ->(host) { new(id: next_meeting_id, host_id: host.id, meeting_time: next_available_day(host)) }
  
  def self.next_meeting_id
    previous_meeting = Meeting.last&.id 
    previous_meeting ? previous_meeting + 1 : 1
  end
  
  def self.next_available_day(host)
    host_time_offset = Time.now.in_time_zone(host.timezone).utc_offset / 3600
    set_host_time = {
       hour: host.meeting_time['hour'] + host_time_offset,
       min: host.meeting_time['minute']
    }
    
    now = DateTime.now.change(set_host_time)
    
    now += 1 + ((host.meeting_day - now.wday) % 7)
  end
  
  def meeting_time=(value)
    host_timezone = Host.find(host_id).timezone
    super(value.to_datetime - Time.now.in_time_zone(host_timezone).utc_offset.seconds)
  end
end
