module FormHelper
  def host_form_options(resource_name)
    {
      as: resource_name,
      url: meeting_path(resource_name),
      html: {
        method: :put,
        class: 'd-flex px-5 pt-3 pb-5 justify-content-center align-items-center flex-wrap'
        }
      }
  end
  
  def user_form_options(resource_name, action_name)
    {
      as: resource_name,
      url: registration_path(resource_name),
      html: {
        method: (action_name == 'edit' ? :put : :post),
        class: 'px-5 pt-3 pb-5'
        }
      }
  end
  
  def available_languages
    languages = I18n.available_locales.sort.map do |locale_key|
      [t("#{locale_key}"), locale_key.to_s]
    end
    
    languages << [t('.other'), 'Other']
  end
  
  def submit_for(action_name)
    (action_name == 'edit' ? t('update') : t('sign_up'))
  end
end