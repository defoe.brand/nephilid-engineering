module MembersHelper
  def member_list
    [
      {
        name: 'Kristina',
        gender: 'female',
        title: 'Administrator',
        avatar: 'avatar-wm.png',
        facebook: '/',
        twitter: '/',
        instagram: '/',
        linkedin: '/'
      },
      {
        name: 'Brandon Defoe',
        gender: 'male',
        title: 'Developer',
        avatar: 'avatar-m1.png',
        facebook: '/',
        twitter: '/',
        instagram: '/',
        linkedin: '/'
      },
      {
        name: 'Mert Gündüz',
        gender: 'male',
        title: 'Developer',
        avatar: 'avatar-m1.png',
        facebook: '/',
        twitter: '/',
        instagram: '/',
        linkedin: '/'
      },
      {
        name: 'Zil Norvilis',
        gender: 'male',
        title: 'Developer',
        avatar: 'avatar-m3.png',
        facebook: '/',
        twitter: '/',
        instagram: '/',
        linkedin: '/'
      },
      {
        name: 'Eduardo Rodriguez',
        gender: 'male',
        title: 'Developer',
        avatar: 'avatar-m2.png',
        facebook: '/',
        twitter: '/',
        instagram: '/',
        linkedin: '/'
      },
      {
        name: 'Daniel Samuel',
        gender: 'male',
        title: 'Developer',
        avatar: 'avatar-m2.png',
        facebook: '/',
        twitter: '/',
        instagram: '/',
        linkedin: '/'
      }
    ]
  end
end
