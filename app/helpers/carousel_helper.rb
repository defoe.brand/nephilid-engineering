module CarouselHelper
  def carousel_items
    [
      {
        image: 'https://i.ibb.co/dWvp1GD/zil.jpg',
        title: 'Zil\'s Portfolio',
        description: 'This is my personal portfolio website.
        Built on React and Ruby on Rails.
        This was built with responsiveness in mind.',
        link: 'https://github.com/zilton7/portfolio',
        alt: 'Zil\'s Portfolio image',
        dark: true
      },
      {
        image: 'https://i.ibb.co/84bcq6f/brandon.jpg',
        title: 'Alien Invasion',
        description: 'The project is a JavaScript based game using the Phaser3 library',
        link: 'https://quirky-nobel-917e25.netlify.app/',
        alt: 'Brandon\'s Alien Invasion image',
        dark: false
      }
    ]
  end
  
  def carousel_scroll_settings(direction)
    {
      type: 'button',
      data: {
        bs_slide: direction,
        bs_target: '#homepage-carousel'
      }
    }
  end
end
