module StudyGroupsHelper
  def component_props(action_name)
    case action_name
      when 'show'
        {
          sessions: @meetings,
          language: I18n.locale,
          locale: current_user&.locale,
          url: @meetings&.first&.meeting_url,
          challenge: @meetings&.first&.challenge_link
        }
      when 'new'
        {username: current_user&.name || 'Guest', language: I18n.locale}
    end
  end

  def transform_datetime_string(datetime, timezone)
    split = datetime.in_time_zone(timezone).to_s.split(' ')
    [split[0], split[1].split(':')[..1].join(':')].join('T')
  end
end
