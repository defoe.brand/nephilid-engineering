module ApplicationHelper
  def toggle_switch_settings(content_id)
    {
      aria: {
        controls: content_id,
        expanded: 'false',
        label: 'Toggle navigation'
      },
      data: {
        bs: {
          target: "##{content_id}",
          toggle: 'collapse'
        }
      },
      type: :button
    }
  end
  
  def next_host_meeting(host)
    Meeting.next_meeting(host).first || Meeting.new_meeting(host)
  end
end
