const language_select = document.getElementById('user_language');

language_select.onchange = () => {
  const selection = language_select.options[language_select.selectedIndex];

  if(selection.value == 'Other'){
    const new_input = document.createElement('input');
    language_select.name = '';
    new_input.type = 'text';
    new_input.classList.add('form-control');
    new_input.classList.add('mt-2');
    new_input.name = 'user[language]';
    language_select.parentElement.appendChild(new_input);
  } else if (language_select.name != 'user[language]'){
    const txt_input = document.querySelector("input[name='user[language]']");
    language_select.name = 'user[language]';
    txt_input.parentElement.removeChild(txt_input);
  }
}