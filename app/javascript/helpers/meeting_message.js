import translate from '../translations/importer'

const display_minutes = (minutes_till, language) => {
  if (minutes_till > 1){
    return `${minutes_till} ${translate[language].minutes}`;
  } else if (minutes_till == 1){
    return `${minutes_till} ${translate[language].minute}`;
  } else {
    return '';
  }
}

const display_hours = (hours_till, language) => {
  const hours = `${(hours_till == 1) ? translate[language].minute : translate[language].minutes}`
  return hours
}

const meeting_today = (hours_till, minutes_till, language) => {
  switch (hours_till) {
    case 0:
      return `${translate[language].next_meeting_in} ${display_minutes(minutes_till, language)}`;
    case 1:
      return `${translate[language].next_meeting_in} ${hours_till} ${translate[language].hour} ${display_minutes(minutes_till, language)}`;
    default:
      return `${translate[language].next_meeting_in} ${hours_till} ${translate[language].hours} ${display_minutes(minutes_till, language)}`; 
  }
}

export default meeting_today