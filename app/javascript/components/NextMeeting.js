import React from 'react'
import Component from 'react'
import PropTypes from 'prop-types'

import meeting_today from '../helpers/meeting_message'

const NextMeeting = ({sessions, location}) => {

  const checkDate = (sessions) => {
    if(sessions.length == 0) { return }

    const next_session = sessions[0].meeting_time

    const today = new Date().getDate()
    const coming_session = new Date(next_session).getDate()

    if(coming_session == today){
    return <h3>Coming Meetings:</h3>
    }
  }

  const coming_meetings = (meeting_times, location, language) => {
    if(meeting_times.length == 0) { return }

    const now = new Date();
    const next_meeting = new Date(meeting_times[0].meeting_time);

    if (next_meeting.getDate() == now.getDate()) {
      const meeting_difference = ((next_meeting - now) % 86400000)
      let hours_till_meeting = Math.floor(meeting_difference / 3600000);
      let minutes_till_meeting = Math.round((meeting_difference % 3600000) / 60000);

      if(minutes_till_meeting == 60){
        hours_till_meeting += 1;
        minutes_till_meeting -= 60;
      }

      return (
        <>
          <p>{meeting_today(hours_till_meeting, minutes_till_meeting, language)}
          <a style={{paddingLeft: 10}} href={meeting_times[0].challenge_link} target='_blank'>Coding Challenge</a></p>
          {coming_meetings(meeting_times.slice(1), location, language)}
        </>
      )

    } else if (next_meeting.getDate() > now.getDate()) {
      const date_options = { weekday: 'long', month: 'long', day: 'numeric' }
      const time_options = { hour: '2-digit', minute: '2-digit' }

      return (
        <ul className='mt-3'>
          <i>Future Meetings:</i>
          {meeting_times.map(session =>
            <li style={{marginLeft: 15, paddingRight: 15}} key={session.meeting_time}>
              {new Date(session.meeting_time).toLocaleDateString(location, date_options)} @ {new Date(session.meeting_time).toLocaleTimeString(location, time_options)}
              <a style={{paddingLeft: 10}} href={session.challenge_link} target='_blank'>Coding Challenge</a>
            </li>
          )}
        </ul>
      )
    }
  }

  return (
    <div className='mt-3'>
      {checkDate(sessions)}
      {coming_meetings(sessions, location.join('-'), location[0])}
    </div>
  );
}

NextMeeting.propTypes = {
  sessions: PropTypes.array,
  location: PropTypes.array
};

export default NextMeeting
