import React from 'react'
import Component from 'react'
import PropTypes from 'prop-types'

import translate from '../translations/importer'

const EnterMeeting = ({language, url, challenge}) => {
  const clickEvent = () => {
    window.open(url)
  }

  return (
    <a href={challenge} onClick={clickEvent}>
      <button id='meeting_button'>{translate[language].join_meeting}</button>
    </a>
  );
}

EnterMeeting.propTypes = {
  language: PropTypes.string,
  url: PropTypes.string
};

export default EnterMeeting
