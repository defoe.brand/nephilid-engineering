// import React, { useState } from 'react'
// import Component from 'react'
// import PropTypes from 'prop-types'
//
// import translate from '../../translations/importer'
//
// const MeetingRoom = ({username, language}) => {
//   console.log(username)
//   console.log(language)
//   const [streamOn, setStreamOn] = useState(false)
//
//   const streamCamVideo = (notStreaming) => {
//     setStreamOn(!streamOn)
//     const video = document.querySelector("video");
//
//     if(notStreaming){
//       var constraints = { audio: true, video: { width: 1280, height: 720 } };
//       navigator.mediaDevices
//       .getUserMedia(constraints)
//       .then((mediaStream) => {
//         video.srcObject = mediaStream;
//         video.onloadedmetadata = function(e) {
//           video.play();
//         };
//       })
//       .catch((err) => {
//         console.log(err.name + ": " + err.message);
//       }); // always check for errors at the end.
//     } else {
//       video.srcObject.getVideoTracks().forEach(track => {
//         track.stop()
//         video.srcObject.removeTrack(track);
//       });
//       video.onloadedmetadata = function(e) {
//         video.stop();
//       };
//     }
//   }
//
//   return (
//     <div className='container d-flex flex-column justify-content-center mt-2'>
//       <button style={{maxWidth: 200, alignSelf: 'center'}} onClick={() => streamCamVideo(!streamOn)}>{streamOn ? "Stop" : "Start"} Streaming</button>
//       <br/>
//       <div className="container d-flex justify-content-center p-0">
//         <video autoPlay={true} id="videoElement" controls></video>
//       </div>
//     </div>
//   );
// }
//
// MeetingRoom.propTypes = {
//   username: PropTypes.string,
//   language: PropTypes.string
// };
//
// export default MeetingRoom


import React from 'react'
import Component from 'react'
import PropTypes from 'prop-types'

import translate from '../../translations/importer'

// import React, { useState } from 'react'
// import Component from 'react'
// import PropTypes from 'prop-types'
//
// import translate from '../../translations/importer'

const MeetingRoom = ({username, language}) => {

  const constraints = {
    'video': {
      'width': {'min': 1280},
      'height': {'min': 720}
    },
    'audio':  {'echoCancellation': true}
  }

  // navigator.mediaDevices.getUserMedia(constraints)
  // .then(stream => {
  //   console.log('Got MediaStream:', stream);
  //   const videoElement = document.querySelector('video#localVideo');
  //   videoElement.srcObject = stream;
  // })
  // .catch(error => {
  //   console.error('Error accessing media devices.', error);
  // });

//   async function makeCall() {
//     const configuration = {'iceServers': [{'urls': 'stun:stun.l.google.com:19302'}]}
//     const peerConnection = new RTCPeerConnection(configuration);
//     signalingChannel.addEventListener('message', async message => {
//         if (message.answer) {
//             const remoteDesc = new RTCSessionDescription(message.answer);
//             await peerConnection.setRemoteDescription(remoteDesc);
//         }
//     });
//     const offer = await peerConnection.createOffer();
//     await peerConnection.setLocalDescription(offer);
//     signalingChannel.send({'offer': offer});
// }

const addPeer = (incomingSignal, callerID, stream) => {
  const peer = new Peer({
    initiator: false,
    trickle: false,
    stream,
  })

  // peer.on("signal", signal => {
  //   socketRef.current.emit("returning signal", {
  //     signal,
  //     callerID
  //   })
  // })

  peer.signal(incomingSignal);

  return peer;
}

const displayImages = Array.from(Array(3).keys())
console.log(displayImages)
// return images
// return 'hello'



  return (
    <div className='d-flex flex-wrap flex-md-nowrap justify-content-start align-items-start'>
    <h1>Welcome {username}</h1>
      <div className='d-flex flex-md-column col-12 col-md-6'>
        <img src='/face.jpg' className='col-6 col-md-12' style={{minWidth: '150px'}}/>
        <img src='/chat.png' className='col-6 col-md-12' />
      </div>
      <div className='d-flex flex-wrap justify-content-around align-items-start'>
      {displayImages.map((x) => {
          return (<>
            <img src='/face.jpg' style={{minWidth: '150px', width: `${100/(displayImages.length*(1 - (1/displayImages.length)))}vw`, maxWidth: '50vw'}}/>
          </>)
      })}
    </div>
  </div>
  );
}
// <video id="localVideo" autoPlay playsInline controls={false}/>

MeetingRoom.propTypes = {
  username: PropTypes.string,
  language: PropTypes.string
};

export default MeetingRoom
