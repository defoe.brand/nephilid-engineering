import React, { useState, useEffect } from 'react'
import Component from 'react'
import PropTypes from 'prop-types'

import NextMeeting from '../NextMeeting'
import EnterMeeting from '../EnterMeeting'

const happening_now = (next_meeting) => {
  const now = new Date();

  if (next_meeting.getDay() == now.getDay()) {
    return (next_meeting.getUTCHours() == now.getUTCHours());
  } else {
    return false;
  }
}

const meeting_display = (next_meeting, sessions, location, url, challenge) => {
  if(happening_now(next_meeting)){

    return (
      <div className='d-flex flex-column justify-content-center align-items-center'>
        <EnterMeeting language={location[0]} url={url} challenge={challenge} />
        <NextMeeting sessions={sessions.slice(1)} location={location}/>
      </div>
    );
  } else {
    return (<NextMeeting sessions={sessions} location={location}/>);
  }
}


const SessionDisplay = ({sessions, language, locale, url, challenge}) => {
  const checkUserSettings = (user_language, user_locale) => {
    const systemSettings = navigator.language.split('-')
    const language = user_language ? user_language : systemSettings[0]
    const locale = user_locale ? user_locale : systemSettings[1]

    return [language, locale]
  }

  const location = checkUserSettings(language, locale)
  const next_meeting = new Date(sessions[0].meeting_time);
  const minute = 6000;

  const [display, setDisplay] = useState(meeting_display(next_meeting, sessions, location, url, challenge));

  useEffect(() => {
    const interval = setInterval(() => {
      setDisplay(meeting_display(next_meeting, sessions, location, url, challenge));
    }, minute);

    return () => clearInterval(interval);
  }, [])

  return (
    <>
      <a href='https://pickone-randomizer.herokuapp.com/' className='container d-flex justify-content-start p-2'>Go to Randomizer!</a>
      <div className="container d-flex justify-content-center mt-2">
        {display}
      </div>
    </>
  );
}

SessionDisplay.propTypes = {
  sessions: PropTypes.array,
  language: PropTypes.string,
  local: PropTypes.string,
  url: PropTypes.string
};

export default SessionDisplay
