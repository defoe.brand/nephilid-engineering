import messages_lt from '../translations/lt.json'
import messages_en from '../translations/en.json'

const translate = {
  'lt': messages_lt,
  'en': messages_en
}

export default translate