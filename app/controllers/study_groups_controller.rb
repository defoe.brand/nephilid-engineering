class StudyGroupsController < ApplicationController  
  def show
    @meetings = Meeting.coming_meetings

    render html: nil, layout: true
  end
  
  def new
    render html: nil, layout: true
  end
end