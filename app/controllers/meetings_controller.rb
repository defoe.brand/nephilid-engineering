class MeetingsController < ApplicationController
  before_action :authenticate_user!

  def show; end
  
  def update
    current_host = current_user.host

    next_meeting = Host.next_meeting(current_host).presence || current_host.meetings.new({host_id: current_host.id}.merge(meeting_params))

    current_host && next_meeting.update(meeting_params)

    redirect_back fallback_location:  root_url
  end
  
  private 

  def meeting_params
    params.require(:meeting).permit(:meeting_url, :meeting_time, :challenge_link)
  end
end