# frozen_string_literal: true

Rails.application.routes.draw do
  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/ do
    root 'static#home'
    get '/about', to: 'static#about'
    devise_for :users
    resource :study_group, only: %i[show]
    resources :meetings, only: %i[update]
  end
end
